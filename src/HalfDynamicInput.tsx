import React from 'react';
import { BackButton } from './BackButton';
import ButtonGroup from '@mui/material/ButtonGroup';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';

export function HalfDynamicInput(): JSX.Element {
	const [userText, setUserText] = React.useState<string>('');
	const [output, setOutput] = React.useState<string>('');
	const [dynamic, setDynamic] = React.useState<boolean>(false);
	const [error, setError] = React.useState<boolean>(false);

	function handleInputChange(
		event: React.ChangeEvent<HTMLInputElement>
	): void {
		setError(false);
		setUserText(event.currentTarget.value);
		if (dynamic) {
			setOutput(event.currentTarget.value);
		}
	}

	function handleGoClick(): void {
		if (userText === '') {
			setError(true);
		} else {
			setOutput(userText);
		}
	}

	function handleDynamicChange(): void {
		setDynamic(!dynamic);
		if (!dynamic) {
			setOutput(userText);
		}
	}

	return (
		<>
			<BackButton to='/exercises' />
			<br />
			<ButtonGroup
				variant='contained'
				aria-label='outlined primary button group'
				fullWidth
			>
				<TextField
					fullWidth
					className='dynamic-input'
					label='Type something'
					onChange={handleInputChange}
					value={userText}
					variant='outlined'
					error={error}
				/>
				{!dynamic && (
					<Button
						className='go-button'
						onClick={handleGoClick}
						style={{ width: 100 }}
						color='success'
					>
						Go
					</Button>
				)}
				<Button
					className='dynamic-button'
					style={{ width: 200 }}
					onClick={handleDynamicChange}
					variant={dynamic ? 'contained' : 'outlined'}
				>
					Dynamic
				</Button>
			</ButtonGroup>
			<div>Output: {output}</div>
		</>
	);
}
