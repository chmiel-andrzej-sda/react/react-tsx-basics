import { shallow, ShallowWrapper } from 'enzyme';
import { personWithoutDataM } from './dataProviders/People';
import { PersonalDetailsComponent } from './PersonalDetailsComponent';

describe('PersonalDetailsComponent', (): void => {
	it('renders', () => {
		// when
		const wrapper: ShallowWrapper = shallow(
			<PersonalDetailsComponent
				personalDetails={personWithoutDataM.personalDetails}
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});
});
