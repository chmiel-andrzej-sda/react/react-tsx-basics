import { PersonalDetails } from './PersonalDetails';
import { TeleAddressData } from './TeleAddressData';

export interface Person {
	readonly personalDetails: PersonalDetails;
	readonly teleAddressData?: TeleAddressData;
}
