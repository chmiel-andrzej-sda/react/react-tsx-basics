export const mockUseParams: jest.Mock = jest.fn();

jest.mock('react-router-dom', () => ({
	useParams: mockUseParams,
	Navigate: (): void => undefined
}));
