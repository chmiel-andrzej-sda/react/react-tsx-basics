import React from 'react';
import TextField from '@mui/material/TextField';

export interface RegexInputFieldProps {
	onChange: (value: RegExp) => void;
	className?: string;
}

export function RegexInputField(props: RegexInputFieldProps): JSX.Element {
	const [error, setError] = React.useState<boolean>(false);

	function handleChange(event: React.ChangeEvent<HTMLInputElement>): void {
		setError(false);
		try {
			props.onChange(new RegExp(event.currentTarget.value));
		} catch (error: unknown) {
			setError(true);
		}
	}

	return (
		<TextField
			fullWidth
			className='regex-input-text-field'
			label='Type some regex'
			variant='outlined'
			error={error}
			onChange={handleChange}
		/>
	);
}
