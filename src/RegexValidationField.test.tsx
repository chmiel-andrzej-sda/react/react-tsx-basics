import { shallow, ShallowWrapper } from 'enzyme';
import { RegexValidationField } from './RegexValidationField';

describe('RegexValidationField', (): void => {
	it('renders', () => {
		// when
		const wrapper: ShallowWrapper = shallow(
			<RegexValidationField value={new RegExp(/abc/)} />
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('handles incorrect value', () => {
		// given
		const wrapper: ShallowWrapper = shallow(
			<RegexValidationField value={new RegExp(/abc/)} />
		);

		// when
		wrapper.find('.regex-validation-text-field').simulate('change', {
			currentTarget: {
				value: 'ddd'
			}
		});

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('handles correct value', () => {
		// given
		const wrapper: ShallowWrapper = shallow(
			<RegexValidationField value={new RegExp(/abc/)} />
		);

		// when
		wrapper.find('.regex-validation-text-field').simulate('change', {
			currentTarget: {
				value: 'abc'
			}
		});

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('handles incorrect and then correct value', () => {
		// given
		const wrapper: ShallowWrapper = shallow(
			<RegexValidationField value={new RegExp(/abc/)} />
		);
		wrapper.find('.regex-validation-text-field').simulate('change', {
			currentTarget: {
				value: 'ddd'
			}
		});

		// when
		wrapper.find('.regex-validation-text-field').simulate('change', {
			currentTarget: {
				value: 'abc'
			}
		});

		// then
		expect(wrapper).toMatchSnapshot();
	});
});
