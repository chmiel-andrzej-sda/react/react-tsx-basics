import { shallow, ShallowWrapper } from 'enzyme';
import { teleAddressData } from './dataProviders/People';
import { TeleAddressDataComponent } from './TeleAddressDataComponent';

describe('TeleAddressDataComponent', (): void => {
	it('renders', () => {
		// when
		const wrapper: ShallowWrapper = shallow(
			<TeleAddressDataComponent teleAddressData={teleAddressData} />
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});
});
