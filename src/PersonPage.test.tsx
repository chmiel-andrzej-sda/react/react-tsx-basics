import { mockUseParams } from './__mocks__/MockReactRouterDom';
import { shallow, ShallowWrapper } from 'enzyme';
import { PersonPage } from './PersonPage';
import { personWithDataM, personWithoutDataM } from './dataProviders/People';

describe('PersonPage', (): void => {
	it('renders correctly without teleaddress data', () => {
		// given
		mockUseParams.mockReturnValue({ id: '0' });

		// when
		const wrapper: ShallowWrapper = shallow(
			<PersonPage people={[personWithoutDataM]} />
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('renders correctly with NaN', () => {
		// given
		mockUseParams.mockReturnValue({ id: 'a' });

		// when
		const wrapper: ShallowWrapper = shallow(
			<PersonPage people={[personWithoutDataM]} />
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('renders correctly with exceeded id', () => {
		// given
		mockUseParams.mockReturnValue({ id: '10' });

		// when
		const wrapper: ShallowWrapper = shallow(
			<PersonPage people={[personWithoutDataM]} />
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('renders correctly with teleaddress data', () => {
		// given
		mockUseParams.mockReturnValue({ id: '0' });

		// when
		const wrapper: ShallowWrapper = shallow(
			<PersonPage people={[personWithDataM]} />
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('renders correctly without id', () => {
		// given
		mockUseParams.mockReturnValue({});

		// when
		const wrapper: ShallowWrapper = shallow(
			<PersonPage people={[personWithoutDataM]} />
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});
});
