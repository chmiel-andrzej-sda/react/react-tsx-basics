import Button from '@mui/material/Button';

export interface SendButtonProps {
	onClick: () => void;
	className?: string;
}

export function SendButton(props: SendButtonProps): JSX.Element {
	return (
		<Button
			className='send-button'
			onClick={props.onClick}
			type='submit'
			color='success'
		>
			SEND
		</Button>
	);
}
