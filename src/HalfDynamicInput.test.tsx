import { shallow, ShallowWrapper } from 'enzyme';
import { HalfDynamicInput } from './HalfDynamicInput';

describe('HalfDynamicInput', (): void => {
	it('renders', () => {
		// when
		const wrapper: ShallowWrapper = shallow(<HalfDynamicInput />);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('changes to dynamic when empty', () => {
		// given
		const wrapper: ShallowWrapper = shallow(<HalfDynamicInput />);

		// when
		wrapper.find('.dynamic-button').simulate('click');

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('changes to dynamic when not empty', () => {
		// given
		const wrapper: ShallowWrapper = shallow(<HalfDynamicInput />);
		wrapper.find('.dynamic-input').simulate('change', {
			currentTarget: {
				value: 'abc'
			}
		});

		// when
		wrapper.find('.dynamic-button').simulate('click');

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('clicking go when empty', () => {
		// given
		const wrapper: ShallowWrapper = shallow(<HalfDynamicInput />);

		// when
		wrapper.find('.go-button').simulate('click');

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('clicking go when not empty', () => {
		// given
		const wrapper: ShallowWrapper = shallow(<HalfDynamicInput />);
		wrapper.find('.dynamic-input').simulate('change', {
			currentTarget: {
				value: 'abc'
			}
		});

		// when
		wrapper.find('.go-button').simulate('click');

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('clicking go when empty and then not empty', () => {
		// given
		const wrapper: ShallowWrapper = shallow(<HalfDynamicInput />);
		wrapper.find('.go-button').simulate('click');
		wrapper.find('.dynamic-input').simulate('change', {
			currentTarget: {
				value: 'abc'
			}
		});

		// when
		wrapper.find('.go-button').simulate('click');

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('handles input', () => {
		// given
		const wrapper: ShallowWrapper = shallow(<HalfDynamicInput />);

		// when
		wrapper.find('.dynamic-input').simulate('change', {
			currentTarget: {
				value: 'abc'
			}
		});

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('handles dynamic input', () => {
		// given
		const wrapper: ShallowWrapper = shallow(<HalfDynamicInput />);
		wrapper.find('.dynamic-button').simulate('click');

		// when
		wrapper.find('.dynamic-input').simulate('change', {
			currentTarget: {
				value: 'abc'
			}
		});

		// then
		expect(wrapper).toMatchSnapshot();
	});
});
