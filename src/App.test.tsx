import App from './App';
import { shallow, ShallowWrapper } from 'enzyme';

describe('App', (): void => {
	it('renders component', () => {
		// when
		const wrapper: ShallowWrapper = shallow(<App />);

		// then
		expect(wrapper).toMatchSnapshot();
	});
});
