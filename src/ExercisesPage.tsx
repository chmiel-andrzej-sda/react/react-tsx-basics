import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import { Link } from 'react-router-dom';
import { BackButton } from './BackButton';

export function ExercisesPage(): JSX.Element {
	return (
		<>
			<BackButton to='/' />
			<br />
			<ButtonGroup
				variant='contained'
				aria-label='outlined primary button group'
			>
				{[1, 2, 3, 4].map(
					(element: number): JSX.Element => (
						<Button
							key={element}
							component={Link}
							to={`/exercises/${element}`}
						>
							{element}
						</Button>
					)
				)}
			</ButtonGroup>
		</>
	);
}
