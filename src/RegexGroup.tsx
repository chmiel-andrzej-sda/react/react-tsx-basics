import React from 'react';
import { BackButton } from './BackButton';
import { RegexInputField } from './RegexInputField';
import { RegexValidationField } from './RegexValidationField';
import ButtonGroup from '@mui/material/ButtonGroup';

export function RegexGroup(): JSX.Element {
	const [text, setText] = React.useState<RegExp>(/.*/);

	return (
		<>
			<BackButton to='/exercises' />
			<ButtonGroup
				variant='contained'
				aria-label='outlined primary button group'
				fullWidth
			>
				<RegexInputField
					className='regex-input-field'
					onChange={setText}
				/>
				<RegexValidationField value={text} />
			</ButtonGroup>
		</>
	);
}
