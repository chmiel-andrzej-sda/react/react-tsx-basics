import { shallow, ShallowWrapper } from 'enzyme';
import { TaskComponent } from './TaskComponent';

describe('TaskComponent', (): void => {
	it('renders task', () => {
		// when
		const wrapper: ShallowWrapper = shallow(
			<TaskComponent
				id={0}
				task={{
					text: 'test task',
					removed: false,
					created: new Date(Date.UTC(2002, 2, 2, 2, 2, 2))
				}}
				onRemoveClick={(): void => undefined}
				onDeleteClick={(): void => undefined}
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('renders removed task', () => {
		// when
		const wrapper: ShallowWrapper = shallow(
			<TaskComponent
				id={0}
				task={{
					text: 'test task',
					removed: true,
					created: new Date(Date.UTC(2003, 3, 3, 3, 3, 3))
				}}
				onRemoveClick={(): void => undefined}
				onDeleteClick={(): void => undefined}
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('handles delete click', () => {
		// given
		const mockDeleteClick: jest.Mock = jest.fn();
		const mockRemoveClick: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<TaskComponent
				id={0}
				task={{
					text: 'test task',
					removed: true,
					created: new Date(Date.UTC(2003, 3, 3, 3, 3, 3))
				}}
				onDeleteClick={mockDeleteClick}
				onRemoveClick={mockRemoveClick}
			/>
		);

		// when
		wrapper.find('.task-component-delete-button').simulate('click');

		// then
		expect(mockDeleteClick).toHaveBeenCalledTimes(1);
		expect(mockDeleteClick).toHaveBeenCalledWith(0);
		expect(mockRemoveClick).toHaveBeenCalledTimes(0);
	});

	it('handles remove click', () => {
		// given
		const mockDeleteClick: jest.Mock = jest.fn();
		const mockRemoveClick: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<TaskComponent
				id={0}
				task={{
					text: 'test task',
					removed: false,
					created: new Date(Date.UTC(2003, 3, 3, 3, 3, 3))
				}}
				onDeleteClick={mockDeleteClick}
				onRemoveClick={mockRemoveClick}
			/>
		);

		// when
		wrapper.find('.task-component-remove-button').simulate('click');

		// then
		expect(mockRemoveClick).toHaveBeenCalledTimes(1);
		expect(mockRemoveClick).toHaveBeenCalledWith(0);
		expect(mockDeleteClick).toHaveBeenCalledTimes(0);
	});

	it('handles remove click when removed', () => {
		// given
		const mockDeleteClick: jest.Mock = jest.fn();
		const mockRemoveClick: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<TaskComponent
				id={0}
				task={{
					text: 'test task',
					removed: true,
					created: new Date(Date.UTC(2003, 3, 3, 3, 3, 3))
				}}
				onDeleteClick={mockDeleteClick}
				onRemoveClick={mockRemoveClick}
			/>
		);

		// when
		wrapper.find('.task-component-remove-button').simulate('click');

		// then
		expect(mockRemoveClick).toHaveBeenCalledTimes(1);
		expect(mockRemoveClick).toHaveBeenCalledWith(0);
		expect(mockDeleteClick).toHaveBeenCalledTimes(0);
	});
});
