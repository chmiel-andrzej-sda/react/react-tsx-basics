export interface TeleAddressData {
	readonly phone: string;
	readonly email: string;
	readonly street: string;
	readonly houseNo: number;
	readonly flatNo: number;
	readonly city: string;
	readonly country: string;
}
