import React from 'react';
import { Navigate } from 'react-router-dom';
import { BackButton } from './BackButton';
import { GenderSelect } from './GenderSelect';
import { Person } from './Person';
import { Gender } from './PersonalDetails';
import { SendButton } from './SendButton';
import { TextInput } from './TextInput';

export interface FormPageProps {
	onSendClick: (person: Person) => number;
}

export function FormPage(props: FormPageProps): JSX.Element {
	const [newId, setNewId] = React.useState<number>(-1);
	const [gender, setGender] = React.useState<Gender>('M');
	const [firstName, setFirstName] = React.useState<string>('');
	const [firstNameError, setFirstNameError] = React.useState<boolean>(false);
	const [middleName, setMiddleName] = React.useState<string>('');
	const [lastName, setLastName] = React.useState<string>('');
	const [lastNameError, setLastNameError] = React.useState<boolean>(false);
	const [pesel, setPesel] = React.useState<string>('');
	const [peselError, setPeselError] = React.useState<boolean>(false);

	function handleSendClick(): void {
		if (firstName === '') {
			setFirstNameError(true);
		}
		if (lastName === '') {
			setLastNameError(true);
		}
		if (pesel === '') {
			setPeselError(true);
		}
		if (firstName !== '' && lastName !== '' && pesel !== '') {
			setNewId(
				props.onSendClick({
					personalDetails: {
						gender,
						firstName,
						middleName,
						lastName,
						pesel
					}
				})
			);
		}
	}

	function handleFormSubmit(event: React.FormEvent) {
		event.preventDefault();
	}

	function handleFirstNameChange(value: string): void {
		setFirstName(value);
		setFirstNameError(false);
	}

	function handleMiddleNameChange(value: string): void {
		setMiddleName(value);
	}

	function handleLastNameChange(value: string): void {
		setLastName(value);
		setLastNameError(false);
	}

	function handlePeselChange(value: string): void {
		setPesel(value);
		setPeselError(false);
	}

	function handleGenderChange(value: Gender): void {
		setGender(value);
	}

	if (newId >= 0) {
		return <Navigate to={`/person/${newId}`} />;
	}

	return (
		<>
			<BackButton to='/person' />
			<br />
			<form
				className='form'
				onSubmit={handleFormSubmit}
			>
				<GenderSelect
					className='gender-select'
					genders={['M', 'K']}
					label='Prosze wybrać płeć'
					value={gender}
					onChange={handleGenderChange}
				/>
				<TextInput
					className='input-first-name'
					label={`Jakie jest ${
						gender === 'M' ? 'Pana' : 'Pani'
					} pierwsze imie?`}
					value={firstName}
					onChange={handleFirstNameChange}
					error={firstNameError}
				/>
				<TextInput
					className='input-middle-name'
					label={`Jakie jest ${
						gender === 'M' ? 'Pana' : 'Pani'
					} drugie imie?`}
					value={middleName}
					onChange={handleMiddleNameChange}
				/>
				<TextInput
					className='input-last-name'
					label={`Jakie jest ${
						gender === 'M' ? 'Pana' : 'Pani'
					} nazwisko?`}
					value={lastName}
					onChange={handleLastNameChange}
					error={lastNameError}
				/>
				<TextInput
					className='input-pesel'
					label={`Jaki jest ${
						gender === 'M' ? 'Pana' : 'Pani'
					} nr PESEL?`}
					value={pesel}
					onChange={handlePeselChange}
					type='number'
					error={peselError}
					maxLength={11}
				/>
				<SendButton
					className='send'
					onClick={handleSendClick}
				/>
			</form>
		</>
	);
}
