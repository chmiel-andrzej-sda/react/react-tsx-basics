import { shallow, ShallowWrapper } from 'enzyme';
import { RegexInputField } from './RegexInputField';

describe('RegexInputField', (): void => {
	it('renders', () => {
		// given
		const mockChange: jest.Mock = jest.fn();

		// when
		const wrapper: ShallowWrapper = shallow(
			<RegexInputField onChange={mockChange} />
		);

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockChange).toHaveBeenCalledTimes(0);
	});

	it('handles incorrect regex', () => {
		// given
		const mockChange: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<RegexInputField onChange={mockChange} />
		);

		// when
		wrapper.find('.regex-input-text-field').simulate('change', {
			currentTarget: {
				value: '/(.+/'
			}
		});

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockChange).toHaveBeenCalledTimes(0);
	});

	it('handles correct regex', () => {
		// given
		const mockChange: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<RegexInputField onChange={mockChange} />
		);

		// when
		wrapper.find('.regex-input-text-field').simulate('change', {
			currentTarget: {
				value: '(.+)'
			}
		});

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockChange).toHaveBeenCalledTimes(1);
		expect(mockChange).toHaveBeenCalledWith(new RegExp(/(.+)/));
	});

	it('handles incorrect and then correct regex', () => {
		// given
		const mockChange: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<RegexInputField onChange={mockChange} />
		);
		wrapper.find('.regex-input-text-field').simulate('change', {
			currentTarget: {
				value: '(.+'
			}
		});

		// when
		wrapper.find('.regex-input-text-field').simulate('change', {
			currentTarget: {
				value: '(.+)'
			}
		});

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockChange).toHaveBeenCalledTimes(1);
		expect(mockChange).toHaveBeenCalledWith(new RegExp(/(.+)/));
	});
});
