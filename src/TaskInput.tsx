import React from 'react';
import { TodoItem } from './TodoItem';
import ButtonGroup from '@mui/material/ButtonGroup';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';

export interface TaskInputProps {
	onAdd: (task: TodoItem) => void;
	className?: string;
}

export function TaskInput(props: TaskInputProps): JSX.Element {
	const [text, setText] = React.useState<string>('');

	function handleChange(event: React.ChangeEvent<HTMLInputElement>): void {
		setText(event.currentTarget.value);
	}

	function handleClick(): void {
		props.onAdd({ text, removed: false, created: new Date() });
		setText('');
	}

	function handleKeyDown(event: React.KeyboardEvent): void {
		if (event.key === 'Enter' && text) {
			handleClick();
		}
	}

	return (
		<ButtonGroup
			variant='contained'
			aria-label='outlined primary button group'
			fullWidth
		>
			<TextField
				fullWidth
				id='outlined-basic'
				label='Type something'
				variant='outlined'
				onKeyDown={handleKeyDown}
				onChange={handleChange}
				value={text}
			/>
			<Button
				className='task-input-button-add'
				variant='outlined'
				disabled={text === ''}
				onClick={handleClick}
				color='success'
				style={{ width: 100 }}
			>
				Add
			</Button>
		</ButtonGroup>
	);
}
