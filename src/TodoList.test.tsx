import { shallow, ShallowWrapper } from 'enzyme';
import { TodoList } from './TodoList';

describe('TodoList', (): void => {
	it('renders', () => {
		// when
		const wrapper: ShallowWrapper = shallow(<TodoList />);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('adds task', () => {
		// given
		const wrapper: ShallowWrapper = shallow(<TodoList />);

		// when
		wrapper.find('.todo-task-input').simulate('add', {
			text: 'test',
			removed: false,
			created: new Date(Date.UTC(2001, 1, 1, 1, 1, 1))
		});

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('deletes task', () => {
		// given
		const wrapper: ShallowWrapper = shallow(<TodoList />);
		wrapper.find('.todo-task-input').simulate('add', {
			text: 'test 1',
			removed: false,
			created: new Date(Date.UTC(2001, 1, 1, 1, 1, 1))
		});

		wrapper.find('.todo-task-input').simulate('add', {
			text: 'test 2',
			removed: false,
			created: new Date(Date.UTC(2001, 1, 1, 1, 1, 1))
		});

		wrapper.find('.todo-task-input').simulate('add', {
			text: 'test 3',
			removed: false,
			created: new Date(Date.UTC(2001, 1, 1, 1, 1, 1))
		});

		// when
		wrapper.find('.todo-items-list').simulate('deleteClick', 1);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('removes task', () => {
		// given
		const wrapper: ShallowWrapper = shallow(<TodoList />);
		wrapper.find('.todo-task-input').simulate('add', {
			text: 'test 1',
			removed: false,
			created: new Date(Date.UTC(2001, 1, 1, 1, 1, 1))
		});

		wrapper.find('.todo-task-input').simulate('add', {
			text: 'test 2',
			removed: false,
			created: new Date(Date.UTC(2001, 1, 1, 1, 1, 1))
		});

		wrapper.find('.todo-task-input').simulate('add', {
			text: 'test 3',
			removed: true,
			created: new Date(Date.UTC(2001, 1, 1, 1, 1, 1))
		});

		// when
		wrapper.find('.todo-items-list').simulate('removeClick', 1);
		wrapper.find('.todo-items-list').simulate('removeClick', 2);

		// then
		expect(wrapper).toMatchSnapshot();
	});
});
