import Button from '@mui/material/Button';
import { Link } from 'react-router-dom';
import { Requestor } from './Requestor';

export function MainPage(): JSX.Element {
	return (
		<>
			<p>
				<Button
					component={Link}
					to='/person'
				>
					Lista ludzi
				</Button>
			</p>
			<p>
				<Button
					component={Link}
					to='/about'
				>
					O stronie
				</Button>
			</p>
			<p>
				<Button
					component={Link}
					to='/exercises'
				>
					Zadania
				</Button>
			</p>
			<br />
			<Requestor />
		</>
	);
}
