import { Person } from '../Person';
import { PersonalDetails } from '../PersonalDetails';
import { TeleAddressData } from '../TeleAddressData';

export const personalDetailsM: PersonalDetails = {
	firstName: 'Test',
	lastName: 'testowy',
	gender: 'M',
	pesel: '9878',
	middleName: 'Test'
};

export const personalDetailsK: PersonalDetails = {
	firstName: 'Test1',
	lastName: 'testowy1',
	gender: 'K',
	pesel: '1234',
	middleName: 'Test1'
};

export const teleAddressData: TeleAddressData = {
	city: 'Krakow',
	country: 'PL',
	email: 'te@s.t',
	flatNo: 10,
	houseNo: 20,
	phone: '123',
	street: 'Testowa'
};

export const personWithDataM: Person = {
	personalDetails: personalDetailsM,
	teleAddressData
};

export const personWithoutDataM: Person = {
	personalDetails: personalDetailsM
};

export const personWithDataK: Person = {
	personalDetails: personalDetailsK,
	teleAddressData
};

export const personWithoutDataK: Person = {
	personalDetails: personalDetailsK
};
