import { shallow, ShallowWrapper } from 'enzyme';
import { AboutPage } from './AboutPage';

describe('AboutPage', (): void => {
	it('renders', () => {
		// when
		const wrapper: ShallowWrapper = shallow(<AboutPage />);

		// then
		expect(wrapper).toMatchSnapshot();
	});
});
