import { arraysMatch, isLengthEven, isPalindromeArray } from './TDD';

describe("TDD", (): void => {
    describe("isLenghtEven", (): void => {
        type TestSet = {
            array?: number[];
            result: boolean;
        };

        [
            {array: undefined, result: false},
            {array: [], result: true},
            {array: [1], result: false},
            {array: [1, 2, 3, 4], result: true}
        ].forEach((set: TestSet): void => {
            test(`${set.array} => ${set.result}`, (): void => {
                // when
                const result: boolean = isLengthEven(set.array);
    
                // then
                expect(result).toBe(set.result);
            });
        });
    });

    describe("isPalindromeArray", (): void => {
        type TestSet = {
            array?: number[];
            result: boolean;
        };

        [
            {array: undefined, result: false},
            {array: [], result: true},
            {array: [1], result: true},
            {array: [1, 1], result: true},
            {array: [1, 2], result: false}
        ].forEach((set: TestSet): void => {
            test(`${set.array} => ${set.result}`, (): void => {
                // when
                const result: boolean = isPalindromeArray(set.array);
    
                // then
                expect(result).toBe(set.result);
            });
        });
    });

    describe("arraysMatch", (): void => {
        type TestSet = {
            source?: number[];
            input?: number[];
            result: boolean;
        };

        [
            {source: undefined, input: undefined, result: false},
            {source: undefined, input: [], result: false},
            {source: [], input: undefined, result: true},
            {source: [], input: [], result: true},
            {source: [], input: [1], result: false},
            {source: [1], input: [], result: true},
            {source: [1, 2, 3], input: [1, 2, 3], result: true},
            {source: [1, 2, 3], input: [1], result: true},
            {source: [1, 2, 3], input: [1, 2, 1, 2, 1, 2, 1, 2], result: true},
            {source: [1, 2, 3], input: [2, 3, 4], result: false}
        ].forEach((set: TestSet): void => {
            test(`${set.source} & ${set.input} => ${set.result}`, (): void => {
                // when
                const result: boolean = arraysMatch(set.source, set.input);
    
                // then
                expect(result).toBe(set.result);
            });
        });
    });
});