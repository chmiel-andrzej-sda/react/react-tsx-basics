import { TaskComponent } from './TaskComponent';
import { TodoItem } from './TodoItem';

export interface TodoItemsListProps {
	tasks: TodoItem[];
	className?: string;
	onRemoveClick: (index: number) => void;
	onDeleteClick: (index: number) => void;
}

export function TodoItemsList(props: TodoItemsListProps): JSX.Element {
	return (
		<ul>
			{props.tasks.map(
				(task: TodoItem, index: number): JSX.Element => (
					<TaskComponent
						className={`task-component-${index}`}
						key={index}
						task={task}
						id={index}
						onRemoveClick={props.onRemoveClick}
						onDeleteClick={props.onDeleteClick}
					/>
				)
			)}
		</ul>
	);
}
