import { shallow, ShallowWrapper } from 'enzyme';
import { personalDetailsM, personWithoutDataM } from './dataProviders/People';
import { FormPage } from './FormPage';

describe('FormPage', (): void => {
	it('renders', () => {
		// given
		const mockSendClick: jest.Mock = jest.fn();

		// when
		const wrapper: ShallowWrapper = shallow(
			<FormPage onSendClick={mockSendClick} />
		);

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockSendClick).toHaveBeenCalledTimes(0);
	});

	it('handles gender change', () => {
		// given
		const mockSendClick: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<FormPage onSendClick={mockSendClick} />
		);

		// when
		wrapper.find('.gender-select').simulate('change', 'K');

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockSendClick).toHaveBeenCalledTimes(0);
	});

	it('handles form send event', () => {
		// given
		const mockSendClick: jest.Mock = jest.fn();
		const mockPreventDefault: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<FormPage onSendClick={mockSendClick} />
		);

		// when
		wrapper.find('.form').simulate('submit', {
			preventDefault: mockPreventDefault
		});

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockSendClick).toHaveBeenCalledTimes(0);
		expect(mockPreventDefault).toHaveBeenCalledTimes(1);
	});

	it('handles incorrect values', () => {
		// given
		const mockSendClick: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<FormPage onSendClick={mockSendClick} />
		);

		// when
		wrapper.find('.send').simulate('click');

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockSendClick).toHaveBeenCalledTimes(0);
	});

	it('handles correct values', () => {
		// given
		const mockSendClick: jest.Mock = jest.fn().mockReturnValue(10);
		const wrapper: ShallowWrapper = shallow(
			<FormPage onSendClick={mockSendClick} />
		);
		wrapper
			.find('.input-first-name')
			.simulate('change', personalDetailsM.firstName);
		wrapper
			.find('.input-last-name')
			.simulate('change', personalDetailsM.lastName);
		wrapper
			.find('.input-middle-name')
			.simulate('change', personalDetailsM.middleName);
		wrapper.find('.input-pesel').simulate('change', personalDetailsM.pesel);

		// when
		wrapper.find('.send').simulate('click');

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockSendClick).toHaveBeenCalledTimes(1);
		expect(mockSendClick).toHaveBeenCalledWith(personWithoutDataM);
	});

	it('handles typing', () => {
		// given
		const mockSendClick: jest.Mock = jest.fn().mockReturnValue(10);
		const wrapper: ShallowWrapper = shallow(
			<FormPage onSendClick={mockSendClick} />
		);

		// when
		wrapper.find('.input-first-name').simulate('change', 'name');
		wrapper.find('.input-last-name').simulate('change', 'last name');
		wrapper.find('.input-middle-name').simulate('change', 'middle name');
		wrapper.find('.input-pesel').simulate('change', '123');

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockSendClick).toHaveBeenCalledTimes(0);
	});

	it('handles correct values', () => {
		// given
		const mockSendClick: jest.Mock = jest.fn().mockReturnValue(10);
		const wrapper: ShallowWrapper = shallow(
			<FormPage onSendClick={mockSendClick} />
		);
		wrapper.find('.send').simulate('click');

		// when
		wrapper.find('.input-first-name').simulate('change', 'name');
		wrapper.find('.input-last-name').simulate('change', 'last name');
		wrapper.find('.input-middle-name').simulate('change', 'middle name');
		wrapper.find('.input-pesel').simulate('change', '123');

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockSendClick).toHaveBeenCalledTimes(0);
	});
});
