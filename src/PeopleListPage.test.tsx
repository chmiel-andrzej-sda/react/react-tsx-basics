import { shallow, ShallowWrapper } from 'enzyme';
import { personWithDataM } from './dataProviders/People';
import { PeopleListPage } from './PeopleListPage';

describe('PeopleListPage', (): void => {
	it('renders', () => {
		// when
		const wrapper: ShallowWrapper = shallow(
			<PeopleListPage people={[personWithDataM]} />
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});
});
