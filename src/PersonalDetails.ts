export type Gender = 'M' | 'K';

export interface PersonalDetails {
	readonly gender: Gender;
	readonly firstName: string;
	readonly middleName?: string;
	readonly lastName: string;
	readonly pesel: string;
}
